let itemService = document.querySelectorAll('.service_item');
let rectangle = document.querySelectorAll('.rectangle')

for (let elem of itemService) {
    elem.onclick = function(){
        let itemActive = document.querySelector('.service_item.service_active');
        let designsContent = document.querySelectorAll('ul.design_content>li');
        for (let el of designsContent) {
            el.style.display = "none";
        }

        let designContent = document.getElementById(elem.dataset.tabs_id);

        designContent.style.display = "block";

        for ( let item of rectangle)   {
            item.classList.add('rectangle_hidden');
        }

        if ( elem !== itemActive) {
            itemActive.classList.remove('service_active');
            
            elem.classList.add('service_active');
        }

        activeRectangle = document.querySelector('.service_item.service_active>img');
        activeRectangle.classList.remove('rectangle_hidden');
    }
}

//--------------------------------------

let itemWork = document.querySelectorAll('.work_item');
// let worksContent = document.querySelectorAll('div.work_content>');
//         console.log(worksContent);


for (let elem of itemWork) {
    elem.onclick = function(){
        let itemActive = document.querySelector('.work_item.work_active');
        let worksContent = document.querySelectorAll('div.work_content>div');
        for (let el of worksContent) {
            if (el.classList.contains(elem.dataset.tabs_class)){
                el.style.display = "block";
            } else {
                el.style.display = "none";
            }
        }

        if ( elem !== itemActive) {
            itemActive.classList.remove('work_active');
            elem.classList.add('work_active');
        }
    }
}

//-----------------------

let my_div = document.getElementById("work_c");
let button = document.getElementById("button_w");
let clickTimes = 0;

button.onclick = function () { 
    clickTimes += 1;

    my_div.innerHTML = my_div.innerHTML + `
    <div class="all web work_content_card">
    <img src="./image/AW0.svg.png" alt="aw0" class="all1 web1">
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Web Design</p>
        </div>
    </div>
</div>
<div class="all landing work_content_card">
    <img src="./image/AW1.png" alt="aw1" class="all1 landing1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Landing Pages</p>
        </div>
    </div>
</div>
<div class="all wordpress work_content_card">
    <img src="./image/AW2.png" alt="aw2" class="all1 wordpress1">
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Wordpress</p>
        </div>
    </div>
</div>                   
<div class="all graphic work_content_card">
    <img src="./image/AW3.png" alt="aw3" class="all1 graphic1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Graphic Design</p>
        </div>
    </div>
</div>
<div class="all wordpress work_content_card">
    <img src="./image/AW4.png" alt="aw4" class="all1 wordpress1">
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Wordpress</p>
        </div>
    </div>
</div>                    
<div class="all graphic work_content_card">
    <img src="./image/AW5.png" alt="aw5" class="all1 graphic1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Graphic Design</p>
        </div>
    </div>
</div>                   
<div class="all web work_content_card">
    <img src="./image/AW6.png" alt="aw6" class="all1 web1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Web Design</p>
        </div>
    </div>
</div>                    
<div class=" all graphic work_content_card">
    <img src="./image/AW7.png" alt="aw7" class="all1 graphic1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Graphic Design</p>
        </div>
    </div>
</div>                   
<div class="all web work_content_card">
    <img src="./image/AW8.png" alt="aw8" class="all1 web1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Web Design</p>
        </div>
    </div>
</div>                    
<div class="all landing work_content_card">
    <img src="./image/AW9.png" alt="aw9" class="all1 landing1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Landing Pages</p>
        </div>
    </div>
</div>                    
<div class="all wordpress work_content_card">
    <img src="./image/AW10.png" alt="aw10" class="all1 wordpress1" >
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Wordpress</p>
        </div>
    </div>
</div>
<div class="all landing work_content_card">
    <img src="./image/AW11.png" alt="aw11" class="all1 landing1">
    <div class="card_hover">
        <img src="./icon/icon.svg" alt="Icon_hover">
        <div class="card_hover_content">
            <p class="card_hover_content1">creative design</p>
            <p class="card_hover_content2">Landing Pages</p>
        </div>
    </div>
</div>
    `
    
    if (clickTimes > 1) {
        button.remove();
    }

    let itemActive = document.querySelector('.work_item.work_active');
    let worksContent = document.querySelectorAll('div.work_content>div');

    for ( let elem of worksContent) {
        if ( elem.classList.contains(itemActive.dataset.tabs_class) ) {
            elem.style.display = "block";
        } else {
            elem.style.display = "none";
        }
    }
}

//----------------------------------------------------

// let date = Date.now();
let date =  new Date();
console.log(date);
let day = date.getDate();
let month = date.toLocaleString('en-US', {month: 'short'});
let cardDay = document.querySelectorAll(".date_d");

for (let elem of cardDay) {
    elem.innerHTML = day;
}

let cardMonth = document.querySelectorAll(".date_m");

for ( let elem of cardMonth) {
    elem.innerHTML = month;
}


// document.getElementById("date").innerHTML = date;

//------------------------------------------------------

document.addEventListener( 'DOMContentLoaded', function () {
    var main = new Splide( '#main-carousel', {
      type      : 'fade',
      rewind    : true,
      pagination: false,
      arrows    : false,
    } );
  
    var thumbnails = new Splide( '#thumbnail-carousel', {
      perPage     : 4,
      padding     : "50px",
      rewind      : true,
      pagination  : false,
      isNavigation: true,
    } );
  
    main.sync( thumbnails );
    main.mount();
    thumbnails.mount();
  } );

let allImg = document.querySelectorAll('.work_content img.all');
